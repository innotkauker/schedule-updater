#!/usr/bin/env ruby

def parse_md(filename)
    blocks = {}
    cur_header = ''

    File.open(filename).each_line do |l|
        if l =~ /^= (.*) =\n$/
            cur_header = $1
            blocks[cur_header] = []
        elsif l =~ /^\s*\* *\[(\d*)\/(\d*)\] *(.*)\n$/
            blocks[cur_header] += [{
                done: $1.to_i,
                total: $2.to_i,
                description: $3
            }]
        elsif l =~ /^\s*\* *\[(.)\] *(.*)\n$/
            blocks[cur_header] += [{
                done: $1 == ' ' ? 0 : 1,
                total: 1,
                description: $2
            }]
        else
            # blank line or something else?
        end
    end
    return blocks
end

def merge(md0, md1)
    [md0, md1].each{|m| m.each{|k,v| v.reject!{|e| e[:done] >= e[:total]} } }
    md0.merge(md1) do |header, e0, e1|
        (e0 + e1).group_by do |entry|
            entry[:description]
        end.map do |descr, entries|
            {
                description: descr,
                done: 0,
                total: entries.reduce(0){|s, e| s + e[:total] - e[:done]}
            }
        end
    end
end

def unparse(md, filename)
    file = File.open(filename, 'w')
    md.each do |header, contents|
        next if contents.empty?
        file.puts("= #{header} =")
        contents.each do |entry|
            if entry[:total] == 1
                file.puts("    * [#{entry[:done] == 1 ? 'X' : ' '}] #{entry[:description]}")
            else
                file.puts("    * [#{entry[:done]}/#{entry[:total]}] #{entry[:description]}")
            end
        end
        file.puts
    end
end

Dir.chdir(File.join(ENV['HOME'], '/vimwiki')) # go to the default vimwiki installation dir
today_sched_file = 'Today schedule.wiki'
tomorrow_sched_file = 'Tomorrow schedule.wiki'
timetable_file = 'Timetable.wiki'
yesterday = `date -d yesterday '+%d.%m.%Y'`.strip
yesterday_dow = `date -d yesterday '+%A'`.strip
today_dow = `date -d today '+%A'`.strip

needed_blocks = [today_dow, 'Daily', 'Standing']
needed_blocks += ['Weekly'] if today_dow == 'Monday'
template = parse_md(timetable_file).select do |header|
    needed_blocks.include? header
end
prev_sched = parse_md(today_sched_file)
prev_sched[today_dow] = prev_sched.delete(yesterday_dow) or []
new_sched = parse_md(tomorrow_sched_file)
new_sched[today_dow] = new_sched.delete('Tomorrow') or []
schedule = merge(template, merge(prev_sched, new_sched))

File.rename(today_sched_file, File.join('results', yesterday))
unparse(schedule, today_sched_file)
File.open(tomorrow_sched_file, 'w').write('= Tomorrow =\n')

# TODO switch to ruby-git?
# TODO support multiple installations
`git add *.wiki "#{today_sched_file}" "#{timetable_file}" "results/#{yesterday}"`
`git commit -m 'Schedule for #{yesterday}'`
`git push origin master`
